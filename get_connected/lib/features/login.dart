import 'package:flutter/material.dart';
import 'package:get_connected/features/dashboard.dart';
import 'package:get_connected/lib/users.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState();

    Users.users[0].interests.add('Infinity Stones');
    Users.users[0].interests.add('Balanced Things');
    Users.users[0].interests.add('Titan');
    Users.users[0].interests.add('Conquest');
  }

  Widget _sizebox(double height) {
    return SizedBox(
      height: height,
    );
  }

  Widget _textfield(String hint) {
    return TextField(
      decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          filled: true,
          hintStyle: TextStyle(color: Colors.grey[800]),
          hintText: hint,
          fillColor: Colors.white70),
    );
  }

  void _nextPage() {
    print('next page');
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => Dashboard()),
    );
  }

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(left: 10.0, right: 10.0),
        color: Color.fromARGB(100, 23, 26, 33),
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Text(
            'Get Connected',
            style: TextStyle(
              color: Colors.white60,
              fontSize: 40,
              fontWeight: FontWeight.w600,
            ),
          ),
          _sizebox(_width * .15),
          _textfield('Username'),
          _sizebox(_width * .02),
          _textfield('Password'),
          _sizebox(_width * .02),
          Container(
            width: double.infinity,
            padding: EdgeInsets.only(right: 10),
            child: Text(
              'Forgot Password?',
              textAlign: TextAlign.end,
              style: TextStyle(
                color: Colors.black,
              ),
            ),
          ),
          _sizebox(_width * .13),
          Container(
            height: 50.0,
            width: _width * .60,
            decoration: BoxDecoration(
                color: Color.fromARGB(100, 199, 213, 224),
                borderRadius: BorderRadius.circular(20.0)),
            child: TextButton(
              onPressed: () {
                _nextPage();
              },
              child: Text('Login',
                  style: TextStyle(
                    color: Colors.white54,
                  )),
            ),
          ),
          _sizebox(_width * .11),
          Container(
            child: Text(
              "Don't have an account yet?",
              style: TextStyle(color: Colors.white54),
            ),
          ),
          _sizebox(_width * .09),
          Container(
            height: 50.0,
            width: _width * .60,
            decoration: BoxDecoration(
                border: Border.all(color: Color.fromARGB(100, 199, 213, 224)),
                borderRadius: BorderRadius.circular(20.0)),
            child: TextButton(
              onPressed: () {},
              child: Text('Create Account',
                  style: TextStyle(
                    color: Colors.white54,
                  )),
            ),
          ),
        ]),
      ),
    );
  }
}
