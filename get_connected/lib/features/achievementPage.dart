import 'package:flutter/material.dart';
import 'package:get_connected/lib/achievements.dart';
import 'package:get_connected/lib/users.dart';

class AchievementList extends StatefulWidget {
  const AchievementList({Key? key}) : super(key: key);

  @override
  _AchievementListState createState() => _AchievementListState();
}

class _AchievementListState extends State<AchievementList> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Center(
            child: Text('Achievements'),
          ),
          automaticallyImplyLeading: false,
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: const Icon(Icons.home),
          backgroundColor: Colors.green,
        ),
        body: Container(
          child: ListView(
            children: [
              for (int i = 0; i < Achievements.achievement.length; i++) ...[
                ListTile(
                  onTap: () {
                    setState(() {
                      Users.users[0].Points +=
                          Achievements.achievement[i].Points;
                      Achievements.achievement[i].isDone = true;
                    });
                  },
                  enabled: !Achievements.achievement[i].isDone,
                  leading: Icon(
                    Icons.star,
                    color: (Achievements.achievement[i].isDone)
                        ? Colors.yellow
                        : Colors.grey,
                  ),
                  title: Text(Achievements.achievement[i].Name),
                )
              ],
            ],
          ),
        ),
      ),
    );
  }
}
