// A widget that displays the picture taken by the user.
import 'dart:io';
import 'package:get_connected/features/dashboard.dart';
import 'package:get_connected/features/questsPage.dart';
import 'package:get_connected/lib/quest.dart';
import 'package:get_connected/lib/users.dart';
import 'package:get_connected/object-detection/classifier.dart';
import 'package:image/image.dart' as img;
import 'package:flutter/material.dart';
import 'package:tflite_flutter_helper/tflite_flutter_helper.dart';

class DisplayPictureScreen extends StatelessWidget {
  final String imagePath;

  late final Category? _category;
  late Classifier classifier;

  late Quest quest;

  File? _image;

  DisplayPictureScreen(
      {Key? key,
      required this.imagePath,
      required this.classifier,
      required this.quest})
      : super(key: key) {
    _predict();

    if (_category!.label == quest.Name) {
      quest.isDone = true;
      Users.users[0].Points += quest.Points;
    }
  }

  void _predict() {
    _image = File(imagePath);
    img.Image imageInput = img.decodeImage(_image!.readAsBytesSync())!;
    var pred = classifier.predict(imageInput);

    this._category = pred;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text('Display the Picture'),
        ),
        automaticallyImplyLeading: false,
      ),
      // The image is stored as a file on the device. Use the `Image.file`
      // constructor with the given path to display the image.
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            flex: 2,
            child: Image.file(_image!),
          ),
          Expanded(
            child: Center(
              child: Container(
                padding: EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      _category!.label,
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    if (quest.isDone) ...[
                      Text(
                        'Congratulations!',
                        textAlign: TextAlign.center,
                      ),
                    ] else ...[
                      Expanded(
                        child: ElevatedButton.icon(
                          label: Text('Retry'),
                          icon: Icon(Icons.replay),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                              Colors.red,
                            ),
                          ),
                        ),
                      ),
                    ],
                    SizedBox(
                      height: 10,
                    ),
                    Expanded(
                      child: ElevatedButton.icon(
                        label: Text('Choose another quest'),
                        icon: Icon(Icons.star),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => QuestList(),
                            ),
                          );
                        },
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(
                            Colors.blue,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Expanded(
                      child: ElevatedButton.icon(
                        label: Text('Home'),
                        icon: Icon(Icons.home),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => Dashboard(),
                            ),
                          );
                        },
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(
                            Colors.green,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
