import 'package:flutter/material.dart';
import 'package:get_connected/lib/users.dart';

class UserProfile extends StatefulWidget {
  UserProfile({Key? key}) : super(key: key);

  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  Widget _verticaldivider() {
    //divider function
    return VerticalDivider(
      color: Colors.black,
      thickness: 1.0,
      indent: 5.0,
      endIndent: 5.0,
    );
  }

  Widget _userinfo(String points, String stats) {
    //convert points to int
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(points),
          Text(stats),
        ],
      ),
    );
  }

  Widget _margin() {
    return SizedBox(
      height: 10.0,
    );
  }

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    double _height = MediaQuery.of(context).size.height;
    return SafeArea(
      child: Scaffold(
        body: Container(
          padding: EdgeInsets.all(10.0),
          child: Column(
            children: [
              Container(
                width: _width * .35,
                height: _height * .16,
                decoration: BoxDecoration(
                  color: Colors.blue,
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      image: AssetImage('img/thanos.jpg'), fit: BoxFit.fill),

                  /// user image
                ),
              ),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      Users.users[0].CharacterName,
                      style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.w600), //user name
                    ),
                    Text(Users.users[0].ChracterID,
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w600)), // user ID
                  ],
                ),
              ),
              _margin(),
              Container(
                //color: Colors.green,
                width: double.infinity,
                height: 40,
                padding: EdgeInsets.only(left: 10.0, right: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    _userinfo('+10', 'Communities'),

                    /// communities
                    _verticaldivider(),
                    _userinfo('+1000', 'Connections'),
                    _verticaldivider(),
                    _userinfo(Users.users[0].Points.toString(), 'Exp Points'),
                  ],
                ),
              ),
              _margin(),
              Container(
                padding: EdgeInsets.all(10.0),
                height: _height * .23,
                width: double.infinity,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                    borderRadius: BorderRadius.circular(20.0)),
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                      "I love infinity stones!", //user bio, add extra text but not necessary
                      style:
                          TextStyle(fontSize: 28, fontWeight: FontWeight.bold)),
                ),
              ),
              _margin(),
              Container(
                width: double.infinity,
                child: Text(
                  'Interests',
                  style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.start,
                ),
              ),
              _margin(),
              Expanded(
                child: Container(
                  child: ListView(
                    children: [
                      for (int i = 0;
                          i < Users.users[0].interests.length;
                          i++) ...[
                        ListTile(
                          leading: Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                          title: Text(Users.users[0].interests[i]),
                        ),
                        if (i < Users.users[0].interests.length - 1) ...[
                          Divider(
                            height: 10,
                          ),
                        ]
                      ]
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
