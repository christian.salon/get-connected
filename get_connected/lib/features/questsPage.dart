import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:get_connected/features/dashboard.dart';
import 'package:get_connected/features/take_picture.dart';
import 'package:get_connected/lib/quests.dart';

class QuestList extends StatelessWidget {
  QuestList({Key? key}) : super(key: key) {
    initCamera();
  }

  late List<CameraDescription> cameras;

  Future<void> initCamera() async {
    WidgetsFlutterBinding.ensureInitialized();

    cameras = await availableCameras();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text(
            'Quests',
          ),
        ),
        automaticallyImplyLeading: false,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => Dashboard(),
            ),
          );
        },
        child: const Icon(Icons.home),
        backgroundColor: Colors.green,
      ),
      body: Container(
        child: ListView(
          children: [
            if (!Quests.isAllDone()) ...[
              for (int quest = 0; quest < Quests.quests.length; quest++) ...[
                if (!Quests.quests[quest].isDone) ...[
                  ListTile(
                    leading: Icon(
                      Icons.star,
                      color: Colors.yellow,
                    ),
                    trailing: Icon(Icons.arrow_forward),
                    title: Text(Quests.quests[quest].Title),
                    onTap: () => {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => TakePictureScreen(
                            camera: cameras[0],
                            quest: Quests.quests[quest],
                          ),
                        ),
                      )
                    },
                  ),
                ] else ...[
                  ListTile(
                    leading: Icon(
                      Icons.star,
                    ),
                    trailing: Icon(Icons.arrow_forward),
                    title: Text(Quests.quests[quest].Title),
                    enabled: false,
                  ),
                ],
              ],
            ] else
              ...[]
          ],
        ),
      ),
    );
  }
}
