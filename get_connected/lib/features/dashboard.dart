import 'package:flutter/material.dart';
import 'package:get_connected/features/achievementPage.dart';
import 'package:get_connected/features/questsPage.dart';
import 'package:get_connected/features/user_map.dart';
import 'package:get_connected/features/userprofile.dart';
import 'package:get_connected/lib/users.dart';

class Dashboard extends StatefulWidget {
  Dashboard({Key? key}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  Widget display(BuildContext context, double height, double width) {
    List<Widget> _forGridView = [
      //listahan para sa mga grids sa
      //grid view
      ElevatedButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => UserProfile()),
          );
        },
        child: Container(
          height: height,
          width: width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(Icons.person),
              Text(
                'Character',
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
      ElevatedButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => QuestList()),
          );
        },
        child: Container(
          height: height,
          width: width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(Icons.star),
              Text(
                'Quests',
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
      ElevatedButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => MapSample()),
          );
        },
        child: Container(
          height: height,
          width: width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(Icons.location_city),
              Text(
                'Discover',
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
      ElevatedButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => AchievementList()),
          );
        },
        child: Container(
          height: height,
          width: width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(Icons.star),
              Text(
                'Achievements',
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    ];
    return Container(
      padding: EdgeInsets.all(10.0),
      height: height * .70,
      child: GridView.count(
        mainAxisSpacing: 20,
        crossAxisSpacing: 20,
        crossAxisCount: 2,
        children: [
          for (int i = 0; i < _forGridView.length; i++) ...[
            _forGridView[i],
          ]
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    return SafeArea(
      child: Scaffold(
        body: Container(
          //padding: EdgeInsets.all(10.0),
          child: Column(
            children: [
              Expanded(
                child: Container(
                  padding: EdgeInsets.fromLTRB(10, 10, 20, 10),
                  color: Colors.black45,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              Users.users[0].CharacterName,
                              style: TextStyle(
                                  fontSize: 25,
                                  fontWeight: FontWeight.w600), //user name
                            ),
                            Text(Users.users[0].ChracterID,
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.w600)),
                          ],
                        ),
                      ),
                      IconButton(
                        icon: Image.asset('img/thanos.jpg'),
                        iconSize: _height * .1,
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => UserProfile()),
                          );
                        },
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                    'My Dashboard',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.w600),
                  ),
                ),
              ),
              Expanded(flex: 6, child: display(context, _height, _width))
            ],
          ),
        ),
      ),
    );
  }
}
