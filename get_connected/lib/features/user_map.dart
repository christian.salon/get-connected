// ignore_for_file: import_of_legacy_library_into_null_safe

import 'dart:async';
import 'package:geolocator/geolocator.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapSample extends StatefulWidget {
  @override
  State<MapSample> createState() => MapSampleState();
}

class MapSampleState extends State<MapSample> {
  Completer<GoogleMapController> _controller = Completer();

  static late Position _currentPosition = Position(
    longitude: 0,
    latitude: 0,
    timestamp: DateTime.now(),
    accuracy: 0,
    altitude: 0,
    heading: 0,
    speed: 0,
    speedAccuracy: 0,
  );
  Set<Marker> _markers = {};
  bool isLoading = true;

  _getCurrentLocation() {
    Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.best,
            forceAndroidLocationManager: true)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
        _markers.add(
          Marker(
            markerId: MarkerId('User'),
            position:
                LatLng(_currentPosition.latitude, _currentPosition.longitude),
            infoWindow: InfoWindow(
              title: 'My Location',
            ),
          ),
        );

        _markers.add(
          Marker(
            markerId: MarkerId('Com-1'),
            position: LatLng(_currentPosition.latitude + 0.0001,
                _currentPosition.longitude + 0.0001),
            infoWindow: InfoWindow(
              title: "commission: PC Repair",
            ),
          ),
        );

        _markers.add(
          Marker(
            markerId: MarkerId('Com-2'),
            position: LatLng(_currentPosition.latitude - 0.0001,
                _currentPosition.longitude + 0.0001),
            infoWindow: InfoWindow(
              title: 'Commission: Math Tutorial',
            ),
          ),
        );

        _markers.add(
          Marker(
            markerId: MarkerId('Com-3'),
            position: LatLng(_currentPosition.latitude + 0.0001,
                _currentPosition.longitude - 0.0001),
            infoWindow: InfoWindow(
              title: 'Commission: Food Delivery',
            ),
          ),
        );

        _markers.add(
          Marker(
            markerId: MarkerId('Com-4'),
            position: LatLng(_currentPosition.latitude - 0.0001,
                _currentPosition.longitude - 0.0001),
            infoWindow: InfoWindow(
              title: 'Event: Worship Service',
            ),
          ),
        );

        _markers.add(
          Marker(
            markerId: MarkerId('Com-5'),
            position: LatLng(_currentPosition.latitude - 0.0002,
                _currentPosition.longitude - 0.0002),
            infoWindow: InfoWindow(
              title: "Food Delivery: Angel's Burger",
            ),
          ),
        );

        isLoading = false;
      });
    }).catchError((e) {
      print(e);
    });
  }

  @override
  void initState() {
    super.initState();
    _getCurrentLocation();
  }

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(_currentPosition.latitude, _currentPosition.longitude),
    zoom: 14.4746,
  );

  static final CameraPosition _kLake = CameraPosition(
      bearing: 192.8334901395799,
      target: LatLng(_currentPosition.latitude, _currentPosition.longitude),
      tilt: 59.440717697143555,
      zoom: 19.151926040649414);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: (isLoading)
          ? Center(child: CircularProgressIndicator())
          : GoogleMap(
              markers: _markers,
              mapType: MapType.hybrid,
              initialCameraPosition: _kGooglePlex,
              onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
              },
            ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: _goToTheLake,
        label: Text('Go to current location'),
        icon: Icon(Icons.person),
      ),
    );
  }

  Future<void> _goToTheLake() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
  }
}
