import 'package:get_connected/lib/achievement.dart';

class Achievements {
  static List<Achievement> achievement = [
    Achievement(
        name: 'Get a romantic partner',
        category: AchievementCategory.ROMANCE,
        points: 75),
    Achievement(
        name: 'Get engaged',
        category: AchievementCategory.ROMANCE,
        points: 100),
    Achievement(
        name: 'Get married',
        category: AchievementCategory.ROMANCE,
        points: 200),
    Achievement(
        name: 'Learn new skill',
        category: AchievementCategory.CAREER,
        points: 100),
    Achievement(
        name: 'Get your first job',
        category: AchievementCategory.CAREER,
        points: 100),
  ];
}
