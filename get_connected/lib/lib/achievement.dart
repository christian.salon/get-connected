class Achievement {
  late String _name;
  late AchievementCategory _category;
  late bool isDone = false;
  late int _points = 1;

  int get Points => _points;
  AchievementCategory get Category => _category;
  String get Name => _name;

  Achievement({
    required String name,
    required int points,
    required AchievementCategory category,
  }) {
    _points = points;
    _name = name;
    _points = points;
    _category = category;
  }
}

enum AchievementCategory {
  ROMANCE,
  CAREER,
  EDUCATION,
  WELL_BEING,
}
