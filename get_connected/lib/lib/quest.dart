class Quest {
  late int _points = 1;
  late String _title;
  late String _name;

  late bool isDone = false;

  String get Title => _title;
  String get Name => _name;
  int get Points => _points;

  Quest(
      {required String title,
      required String name,
      required QuestCategory category}) {
    isDone = false;
    _title = title;
    _name = name;
    _points *= category.index;
  }
}

enum QuestCategory { ONE, TWO, THREE, FOUR }
