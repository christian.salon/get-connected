class User {
  late String _username;
  late String _password;
  late String _characterName;
  late String _characterID;

  List<String> interests = [];

  String get Username => _username;
  String get Password => _password;
  String get CharacterName => _characterName;
  String get ChracterID => _characterID;

  late int Points;

  User(
      {required String Username,
      required String Password,
      required String CharacterName,
      required String CharacterID,
      required int Points}) {
    _username = Username;
    _password = Password;
    _characterName = CharacterName;
    _characterID = CharacterID;
    this.Points = Points;
  }
}
