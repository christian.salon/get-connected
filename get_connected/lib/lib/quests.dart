import 'package:get_connected/lib/quest.dart';

class Quests {
  static bool isAllDone() {
    int countDone = 0;
    quests.forEach((quest) {
      if (quest.isDone) countDone++;
    });

    return (countDone > 3) ? true : false;
  }

  static List<Quest> quests = [
    Quest(
      title: 'Get a Cup of Coffee',
      name: 'cup',
      category: QuestCategory.ONE,
    ),
    Quest(
      title: 'Read a notebook',
      name: 'notebook',
      category: QuestCategory.ONE,
    ),
    Quest(
      title: 'Get a Desk',
      name: 'desk',
      category: QuestCategory.TWO,
    ),
    Quest(
      title: 'Wear a sunglass',
      name: 'sunglass',
      category: QuestCategory.TWO,
    ),
    Quest(
      title: 'Get a Desktop Computer',
      name: 'cup',
      category: QuestCategory.THREE,
    ),
    Quest(
      title: 'Use/Buy a Laptop',
      name: 'laptop',
      category: QuestCategory.THREE,
    ),
    Quest(
      title: 'Wear Swimming Trunks',
      name: 'swimming trunks',
      category: QuestCategory.THREE,
    ),
  ];
}
